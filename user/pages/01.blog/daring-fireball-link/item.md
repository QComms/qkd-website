---
title: 'Integrated QKD'
taxonomy:
    category:
        - blog
    tag:
        - demo
        - cryptography
hero_classes: 'text-light title-h1h2 overlay-dark hero-large parallax'
hero_image: oclaro.jpg
show_sidebar: true
feed:
    limit: 10
---

Integrated Photonics provide a stable, compact, miniaturized and robust platform to implement complex photonic circuits amenable to manufacture and therefore provide a compelling technology to implement future QKD systems. 
```
link: https://arxiv.org/pdf/1612.07236.pdf
```
===

### Integrated QKD in Bristol

Quantum Key Distribution (QKD) provides a provably secure approach to share keys used to encrypt secret information by transmitting single photons. This is one of the first commercial quantum technologies available, and Bristol is heavily involved in a recently awarded Quantum Technology Hub in Quantum Cryptography (along with York, Cambridge, Leeds and others) [1].
Integrated Photonics provide a stable, compact, miniaturized and robust platform to implement complex photonic circuits amenable to manufacture and therefore provide a compelling technology to implement future QKD systems. The phase stability of integrated photonics allows for a suitable platform for time-bin encoded information, used in telecommunication QKD for fibre communications.
Current technologies Bristol has demonstrated include: integrated Indium Phosphide devices for QKD transmitters at GHz rates for telecommunications channels that incorporate integrated lasers, electro-optic modulators and photodiodes partnered with Silicon Oxy-Nitride receivers; and Silicon devices capable of encoding the necessary QKD signals at GHz speeds.
![](zintegrated.jpg)
![](zzfromID2Kets.jpg)

[1] EPSRC, "Quantum Leap," 2014. [Online]. Available: [http://www.epsrc.ac.uk/newsevents/news/quantumtechhubs/](http://www.epsrc.ac.uk/newsevents/news/quantumtechhubs/)