---
title: 'What is QKD?'
hero_classes: 'text-light title-h1h2 overlay-dark-gradient hero-large parallax'
hero_image: cover_user.jpg
show_sidebar: true
taxonomy:
    category: blog
    tag:
        - education
---

Rather than a single vulnerable key (such as a password), Quantum Key Distribution (QKD) systems and specialist software can provide a continuous supply of provably secure keys.

===

## Quantum Key Distribution

[Quantum Key Distribution](https://en.wikipedia.org/wiki/Quantum_key_distribution) (QKD) is a way to share randomness between 2 parties at 2 different locations in a way that is provably secure.

## What are the advantages of QKD symmetric encryption over public key and asymmetric encryption?

Public key is vulnerable to the computing power of quantum computers and their ability to reverse engineer the key. A quantum computer could solve the key within a practical time, such as hours or days, depending on the available power of the computer. Symmetric encryption is much less vulnerable but has been impractical so far. Because some symmetric encryption schemes use a specific numerical key, it would take a lot of time and effort to guess the key. Even with a supercomputer, it could take [more than a lifetime](https://www.eetimes.com/document.asp?doc_id=1279619) to crack the key.

QKD allows a new approach to symmetric encryption by enabling users to generate shared, truly random symmetric keys from different locations on the network. A database of keys is built up continuously in the key store and a new key can be used for each data transfer. This communication link can detect an eavesdropper, thereby preventing interception of the key. Symmetric encryption via QKD thus allows long term secrecy. 
