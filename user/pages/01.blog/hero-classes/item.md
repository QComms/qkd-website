---
title: 'What is a Quantum State?'
taxonomy:
    category:
        - blog
    tag:
        - education
hero_classes: 'text-light title-h1h2 overlay-dark-gradient hero-large parallax'
hero_image: in1.jpg
show_sidebar: true
feed:
    limit: 10
author: 'Tasha Maxwell'
---

A [Quantum State](https://en.wikipedia.org/wiki/Quantum_state) is the state of a quantized system. In "layman's terms", it is simply something that `encodes the state of a system`. A special feature of such systems is that they can be simultaneously in a superposition of several such quantum states which enable interesting features. A quantum state can actually contain all the information about a given system.


===

## State representation: the Bloch sphere

Quantum information encodes what is called qubits, much like classical information it can take 2 values '0' and '1' but those values correspond to eigen values of a vector in an Hilbert space so a quantum state can also be in a superposition of those two:
![](qstate.jpg?resize=1200,80)
where alpha and beta are probability amplitudes. This imply that a qubit can actually define an infinity number of states. This can be seen in the Bloch-Poincaré sphere representation in the fact that the arrow of the vector can aim in any possible direction.

Those properties of the quantum state, together with complex algebra, are what is enabling quantum information to go beyond the capabilities of classical information in some specific protocols and applications.
![](zbloch.jpg)
