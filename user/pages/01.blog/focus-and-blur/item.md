---
title: 'Handheld QKD'
hero_classes: 'text-light title-h1h2 overlay-dark hero-large parallax'
hero_image: QKD_ATM.jpg
show_sidebar: true
taxonomy:
    category: blog
    tag:
        - demo
        - cryptography
---

Our solution is analogous to a consumer visiting an ATM if they require cash, they will visit a ``Quantum ATM’’ if they require more key material.

===

## QKD solutions: from central data center to personal encryption

 Fibre optic quantum networks can exchange keys (QKD) over national and metropolitan scales however this leaves out individual parties who may wish to obtain keys for private communications, for example over the internet.
The fibre optic hardware is expensive and also requires dedicated infrastructure. We have produced a miniaturized QKD transmitter device which can exchange keys with a larger fixed terminal. Focusing on the transmitter allows for many functions of the entire system to be concentrated into the receiver.
Our arrangement is analogous to a consumer visiting an ATM if they require cash, they will visit a ``Quantum ATM’’ if they require more key material. The Quantum ATM will be co-located with a node of a wider quantum network and can act as a trusted node to allow the user to exchange keys between any other node, including users of other Quantum ATMs.

The size reduction of the transmitter is achieved by using LEDs as weak pulsed single photon sources which are easier to drive than laser diodes. The LEDs are individually polarized to states for the BB84 QKD protocol and combined using a short length single mode fibre to ensure that the light from the LEDs is indistinguishable. A mechanical dock is used to align the transmitter to the receiver over a short free space channel.
The Quantum ATM contains a QKD receiver with detectors in all three bases, this allows for the receiver to calibrate individual transmitters to align their reference frames. Single photon avalanche diodes (SPADs) are used to detect the photons, the signals from these are passed to an FPGA based time tagger which sends digital information for processing.
The processing protocol is optimized for the situation where there as an asymmetric distribution of computing hardware between the two parties. The transmitter is controlled by a small embedded or system-on-chip computer whereas the receiver terminal can have a powerful regular PC inside of it.
