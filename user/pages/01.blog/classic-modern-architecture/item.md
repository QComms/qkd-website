---
title: 'Quantum-Safe Health records'
taxonomy:
    category:
        - blog
    tag:
        - demo
        - cryptography
hero_classes: 'text-dark title-h1h2 overlay-light hero-large parallax'
hero_image: HealthSafe.jpg
blog_url: /blog
show_sidebar: true
---

Symmetric encryption via QKD allows, amongst other things, long term secrecy. Naturally, a consummer oriented application leveraging that feature would be securing personnal informations like health records.

===

### Typical scenario for usage of a Trusted-node QKD network infrastructure
[QKD_KeyExchange_Storyboard](/book)