---
title: 'CQP Toolkit'
hero_classes: 'text-light title-h1h2 overlay-dark hero-large parallax'
hero_image: ToolkitPIC.jpg
show_sidebar: true
taxonomy:
    category: blog
    tag:
        - demo
        - cryptography
        - software
---

The purpose of this software, [CQP Toolkit](https://qcomms.gitlab.io/cqptoolkit/), is to provide a set of interfaces, algorithms and tools to produce a full QKD system. The aim is to reduce the barrier to entry into QKD systems by developing a strong architecture with well defined interfaces. The separation of responsibility into components will allow researchers to concentrate on a specific component, the rest of the system being provided by existing algorithms or simulators.


### Bristol University CQP Quantum Key Distribution Toolkit

The project can be downloaded [here](https://gitlab.com/QComms/cqptoolkit).

 Researchers in the Quantum Technology Engineering Labs (QETLabs) have developed open source software, ‘CQP Toolkit’, capable of incorporating various QKD solutions into current networking infrastructures. Designed to incorporate different QKD solutions into current networking infrastructures, the toolkit allows for driving, testing devices and provides access to the raw data. Leveraging existing technology, the CQP Toolkit can be used with many programming languages and platforms. It can manage keys from different sources to experiment with different uses for symmetric keys, i.e. create encrypted custom VPN tunnels between computers with the QTunnel program.
 
For the key to be useful, it must be compatible with available communication protocols like today’s modern web browsers. It is of crucial importance to show integration with existing technologies such as TLS since this protocol is allowing a server and a client to communicate securely. This is usually done with an exchange of public keys but more recent version like TLS 1.2 comes with a PSK mode allowing the client to negotiate a “pre-shared key”. 

Various implementations of the TLS protocol exist but many assume only one key is available making them incompatible with QKD systems. However, by integrating with the OpenSSL library we are showing that a secured web server at “We The Curious” and a client using a WIFI access point, at another location of the “trusted nodes” QKD network, can successfully negotiate a connection thus providing a Quantum enabled TLS connection.
