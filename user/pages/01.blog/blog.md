---
title: Home
sitemap:
    changefreq: monthly
body_classes: 'header-dark header-transparent'
hero_classes: 'text-light title-h1h2 overlay-dark-gradient hero-large parallax'
hero_image: QKDover5GUK.jpg
hero_align: left
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
content:
    items:
        - '@self.children'
    limit: 6
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
feed:
    limit: 10
    description: 'Quantum Safe Web Portal Description'
custom: 'new thing'
pagination: true
---

# QKD over 5GUK #
## Web portal secured by QKD ##