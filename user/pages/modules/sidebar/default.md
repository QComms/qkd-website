---
routable: false
---

#### Quantum Engineering Technology Labs
The mission of QETLabs is to take quantum science discoveries out of the labs and engineer them into technologies for the benefit of society. It brings together `£50M worth of activity` that covers theoretical quantum physics through experiment, engineering and skills and training toward concept demonstrators of quantum technologies. 



